package chess93.androidchess93.listener;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.widget.GridView;

import chess93.androidchess93.activity.ChessActivity;
import chess93.androidchess93.model.Space;

//Select piece and then select target location (will move if valid otherwise will deselect)
public class SpaceTouchListener implements View.OnDragListener{

    private ChessActivity chessActivity;
    private Space[][] board;
    private GridView gridView;
    private Context context;
    private int barHeight;
    private int initFile, initRank, finalFile, finalRank;

    public SpaceTouchListener(Space[][] board, GridView gv, int barHeight, Context ctx) {
        this.board = board;
        this.gridView = gv;
        this.barHeight = barHeight;
        this.context = ctx;
        this.chessActivity = (ChessActivity) context;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int x,y;

        switch(event.getAction())
        {
            case DragEvent.ACTION_DRAG_STARTED:

                x = (int) event.getX();
                y = (int) event.getY();
                y -= barHeight;
                initFile = gridView.pointToPosition(x, y) % 8;
                initRank = Math.abs((gridView.pointToPosition(x, y) - initFile)/8 - 7);
                if (!board[initFile][initRank].hasPiece()) {
                    return false;
                }
                break;

            case DragEvent.ACTION_DROP:
                finalFile = gridView.getPositionForView(v) % 8;
                finalRank = Math.abs((gridView.getPositionForView(v) - finalFile)/8 - 7);
                if (initFile == finalFile && initRank == finalRank)
                    return true;
                chessActivity.move(initFile, initRank, finalFile, finalRank);
                break;
            default: break;
        }
        return true;
    }
}
