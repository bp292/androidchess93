package chess93.androidchess93.model;

import chess93.androidchess93.R;

public class Queen extends MoveLong {

    public Queen(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {
        //check if valid long movement
        if ((Math.abs(initFile - finalFile) == Math.abs(initRank - finalRank)
                || ((initFile == finalFile && initRank != finalRank) || (initFile != finalFile && initRank == finalRank)))
                && !hasPiecesInbetween(initFile, initRank, finalFile, finalRank, board))
        {
            if (board[finalFile][finalRank].getPiece() == null
                    || board[finalFile][finalRank].getPiece().isWhite() != isWhite())
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public int getDrawable() {
        if (isWhite()) {
            return R.drawable.ic_white_queen;
        }
        else {
            return R.drawable.ic_black_queen;
        }
    }
}
