package chess93.androidchess93.model;


import android.content.Context;
import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RecordGameList implements Serializable {

    private static final long serialVersionUID = 9221355046218690511L;
    public static final String storeFile = "games";

    private List<RecordGame> games;

    public RecordGameList() {
        games = new ArrayList<RecordGame>();
    }

    public List<RecordGame> getGamesList()
    {
        return games;
    }

    public void addGameToList(RecordGame r)
    {
        games.add(r);
    }

    public void removeGameFromList(RecordGame r)
    {
        games.remove(r);
    }

    public boolean gameTitleExists(String r)
    {
        for(RecordGame g: games)
        {
            if (g.getTitle().equals(r)) {
                return true;
            }
        }
        return false;
    }

    public RecordGame getGameByTitle(String r)
    {
        for(RecordGame g: games)
        {
            if (g.getTitle().equals(r))
                return g;
        }
        return null;
    }

    public String toString() {
        if (games == null)
            return "No Recorded Games Available To View";
        String output = "";
        for(RecordGame u : games)
        {
            output += u.getTitle() + " ";
        }
        return output;
    }

    public static RecordGameList read(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(storeFile);
        ObjectInputStream ois = new ObjectInputStream(fis);
        RecordGameList glist = (RecordGameList) ois.readObject();
        ois.close();
        return glist;
    }

    public static void write (Context context, RecordGameList glist) throws IOException {
        FileOutputStream fos = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(glist);
        oos.close();
        fos.close();
    }


}

