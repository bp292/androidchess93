package chess93.androidchess93.model;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import chess93.androidchess93.R;
//Imageview issue, use AppCompatImageView ibnstead?
public class Space extends ImageView implements Cloneable {

    private ChessPiece occupyingChessPiece;
    private boolean isWhite;

    public Space(Context c, boolean isWhite)
    {
        this(c, null, isWhite);
    }

    public Space(Context c, ChessPiece chessPiece, boolean isWhite) {
        super(c);
        setPiece(chessPiece);
        this.isWhite = !isWhite;

        int colorID = isWhite ? R.color.whiteSquare : R.color.blackSquare;
        setBackgroundColor(getResources().getColor(colorID));
    }

    public Space(Context c, AttributeSet attrs) {
        super(c, attrs);
    }

    public Space(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Sets the chessPiece occupying the square with the parameter
     * @param chessPiece - the new chessPiece occupying the square
     */
    public void setPiece(ChessPiece chessPiece) {
        this.occupyingChessPiece = chessPiece;

        if (occupyingChessPiece == null)
            setImageDrawable(null);
        else {
            setImageResource(occupyingChessPiece.getDrawable());
            setScaleType(ScaleType.CENTER_INSIDE);
        }
    }

    /**
     * occupyingChessPiece Accessor
     * @return the occupying piece
     */
    public ChessPiece getPiece() {
        return occupyingChessPiece;
    }

    /**
     * Checks to see if there exists an occupyingChessPiece on the square
     * @return true if there is an occupying piece, else false
     */
    public boolean hasPiece() {
        return occupyingChessPiece != null;
    }

    /**
     * isWhite Accessor
     * @return true if square is white, false is square is black
     */
    public boolean isSquareWhite() {
        return isWhite;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
    {
        final int width = getDefaultSize(getSuggestedMinimumWidth(),widthMeasureSpec);
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
    {
        super.onSizeChanged(w, w, oldw, oldh);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
