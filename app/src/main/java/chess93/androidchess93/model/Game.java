package chess93.androidchess93.model;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import chess93.androidchess93.R;

public class Game {

    static final boolean WHITE = true;
    static final boolean BLACK = false;

    private Space[][] board;

    private Space[][] clone;

    private boolean isDone,
        isStalemate,
        isCheckmate,
        isResign,
        isWhiteWinner,
        isWhitesMove,
        isWhiteInCheck,
        isBlackInCheck,
        isInCheck,
        isDrawAvailable,
        recordedPromotion;
        int[] recordedPromotionPos;

    private Location whiteKingLocation, blackKingLocation;

    private List<RecordMove> recordedMoves;

    private Context context;

    public Game(Context context) {
        board = new Space[8][8];

        this.context = context;

        // Initialize first rank of Black's side
        board[0][7] = new Space(context, new Rook(BLACK), WHITE);
        board[7][7] = new Space(context, new Rook(BLACK), BLACK);
        board[1][7] = new Space(context, new Knight(BLACK), BLACK);
        board[6][7] = new Space(context, new Knight(BLACK), WHITE);
        board[2][7] = new Space(context, new Bishop(BLACK), WHITE);
        board[5][7] = new Space(context, new Bishop(BLACK), BLACK);
        board[3][7] = new Space(context, new Queen(BLACK), BLACK);
        board[4][7] = new Space(context, new King(BLACK), WHITE);

        // Initialize first rank of White's side
        board[0][0] = new Space(context, new Rook(WHITE), BLACK);
        board[7][0] = new Space(context, new Rook(WHITE), WHITE);
        board[1][0] = new Space(context, new Knight(WHITE), WHITE);
        board[6][0] = new Space(context, new Knight(WHITE), BLACK);
        board[2][0] = new Space(context, new Bishop(WHITE), BLACK);
        board[5][0] = new Space(context, new Bishop(WHITE), WHITE);
        board[3][0] = new Space(context, new Queen(WHITE), WHITE);
        board[4][0] = new Space(context, new King(WHITE), BLACK);

        int file, rank;

        // Initialize all black pawns
        rank = 6;
        for (file = 0; file < 8; file++) {
            if (file%2 == 0)
                board[file][rank] = new Space(context, new Pawn(BLACK), BLACK);
            else
                board[file][rank] = new Space(context, new Pawn(BLACK), WHITE);
        }

        // Initialize all empty squares
        for (rank = 5; rank >=2; rank--) {
            for (file = 0; file < 8; file++) {
                if ((file%2 == 0 && rank%2 == 0) || (file%2 != 0 && rank%2 != 0))
                    board[file][rank] = new Space(context, BLACK);
                else
                    board[file][rank] = new Space(context, WHITE);
            }
        }

        // Initialize all white pawns
        rank = 1;
        for (file = 0; file < 8; file++) {
            if (file%2 == 0)
                board[file][rank] = new Space(context, new Pawn(WHITE), WHITE);
            else
                board[file][rank] = new Space(context, new Pawn(WHITE), BLACK);
        }

        // Initialize all private booleans
        isWhitesMove = true;
        isDone = false;
        isStalemate = false;
        isCheckmate = false;
        isResign = false;
        isWhiteWinner = false;
        isWhiteInCheck = false;
        isBlackInCheck = false;
        isInCheck = false;
        isDrawAvailable = false;
        recordedPromotion = false;
        recordedPromotionPos = new int[1];
        recordedPromotionPos[0] = -1;

        recordedMoves = new ArrayList<RecordMove>();

        whiteKingLocation = new Location(4,0);
        blackKingLocation = new Location(4,7);

        clone = createClone(board);
    }

    public Space[][] getBoard() {
        return board;
    }

    public boolean isValidPieceMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] gameBoard) {
        if (initFile == finalFile && initRank == finalRank) {
            return false;
        }
        if (!gameBoard[initFile][initRank].hasPiece()) {
            return false;
        }
        if (gameBoard[initFile][initRank].getPiece().isWhite() != isWhitesMove()) {
            return false;
        }

        return gameBoard[initFile][initRank].getPiece().isValidMove(initFile, initRank, finalFile, finalRank, gameBoard);
    }

    public boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank) {

        if (isValidPieceMove(initFile, initRank, finalFile, finalRank, board)) {
            pieceMove(initFile, initRank, finalFile, finalRank, clone);
            boolean kingIsInCheck = isKingInCheck(clone, !isWhitesMove());
            undo(clone);
            return !kingIsInCheck;
        }

        return false;
    }

    public void pieceMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] gameBoard) {
        RecordMove recordedMove = gameBoard[initFile][initRank].getPiece().move(initFile, initRank, finalFile, finalRank, gameBoard);

        recordedMoves.add(recordedMove);

        if (gameBoard[finalFile][finalRank].getPiece() instanceof King) {
            if (gameBoard[finalFile][finalRank].getPiece().isWhite()) {
                whiteKingLocation.setFile(finalFile);
                whiteKingLocation.setRank(finalRank);
            } else {
                blackKingLocation.setFile(finalFile);
                blackKingLocation.setRank(finalRank);
            }
        }

        if (gameBoard[finalFile][finalRank].getPiece() instanceof Pawn) {
            // check for promotion
            if (isWhitesMove() && finalRank == 7) {
                promotion(initFile, initRank, finalFile, finalRank, gameBoard, recordedMove);
            }
            if (!isWhitesMove() && finalRank == 0) {
                promotion(initFile, initRank, finalFile, finalRank, gameBoard, recordedMove);
            }
        }

        changePlayer();
    }

    public void move(int initFile, int initRank, int finalFile, int finalRank) {

        pieceMove(initFile, initRank, finalFile, finalRank, board);
        createClone();
        testCheck();

        recordedMoves.get(recordedMoves.size() - 1).setInCheck(isInCheck);
        recordedMoves.get(recordedMoves.size() - 1).setInCheckmate(isCheckmate);
        recordedMoves.get(recordedMoves.size() - 1).setInStalemate(isStalemate);
    }

    public void undo(Space[][] board) {

        RecordMove recordedMove = recordedMoves.remove(recordedMoves.size() - 1);

        //Log.d("TEST", board[recordedMove.getMovingInitFile()][recordedMove.getMovingInitRank()]
        //        .hasPiece() + "");

        if (recordedMove.isPromotion())
            board[recordedMove.getMovingFinalFile()][recordedMove.getMovingFinalRank()]
                    .setPiece(recordedMove.getDeletedChessPiece());

        // move piece back to it's original square
        board[recordedMove.getMovingInitFile()][recordedMove.getMovingInitRank()]
                .setPiece(board[recordedMove.getMovingFinalFile()][recordedMove.getMovingFinalRank()].getPiece());

        if (board[recordedMove.getMovingInitFile()][recordedMove.getMovingInitRank()].getPiece() instanceof King) {
            if (board[recordedMove.getMovingInitFile()][recordedMove.getMovingInitRank()].getPiece().isWhite()) {
                whiteKingLocation.setFile(recordedMove.getMovingInitFile());
                whiteKingLocation.setRank(recordedMove.getMovingInitRank());
            }
            else {
                blackKingLocation.setFile(recordedMove.getMovingInitFile());
                blackKingLocation.setRank(recordedMove.getMovingInitRank());
            }
        }

        // remove the duplicate piece
        board[recordedMove.getMovingFinalFile()][recordedMove.getMovingFinalRank()].setPiece(null);
        if (!recordedMove.isPromotion()) {

            // revert piece back to it's previous hasMoved value
            board[recordedMove.getMovingInitFile()][recordedMove.getMovingInitRank()]
                    .getPiece().setHasMoved(recordedMove.hasPreviouslyMoved());

            // place recovered piece
            board[recordedMove.getDeletedFile()][recordedMove.getDeletedRank()]
                    .setPiece(recordedMove.getDeletedChessPiece());
        }

        // check for castling
        if (recordedMove.getCastledInitFile() != -1) {
            // revert the rook
            board[recordedMove.getCastledInitFile()][recordedMove.getCastledinitRank()]
                    .setPiece(board[recordedMove.getCastledFinalFile()][recordedMove.getCastledFinalRank()].getPiece());

            // remove duplicate piece
            board[recordedMove.getCastledFinalFile()][recordedMove.getCastledFinalRank()].setPiece(null);

            // revert piece back to a hasMoved value of false
            board[recordedMove.getCastledInitFile()][recordedMove.getCastledinitRank()].getPiece().setHasMoved(false);
        }

        // go back to previous player's turn
        changePlayer();

    }

    public List<Pair<Location, Location>> getAllValidMoves() {
        List<Pair<Location, Location>> allMoves = new ArrayList<Pair<Location, Location>>();
        Pair<Location, Location> move = null;

        for (int initFile = 0; initFile < 8; initFile++) {
            for (int initRank = 0; initRank < 8; initRank++) {
                for (int finalFile = 0; finalFile < 8; finalFile++) {
                    for (int finalRank = 0; finalRank < 8; finalRank++) {
                        if (isValidMove(initFile, initRank, finalFile, finalRank)) {
                            move = new Pair<Location,Location>(new Location(initFile, initRank), new Location(finalFile, finalRank));
                            allMoves.add(move);
                        }
                    }
                }
            }
        }

        return allMoves;
    }

    public boolean isKingInCheck(Space[][] gameBoard, boolean isKingWhite) {
        int kingFile, kingRank;
        if (isKingWhite) {
            kingFile = whiteKingLocation.getFile();
            kingRank = whiteKingLocation.getRank();
        }
        else {
            kingFile = blackKingLocation.getFile();
            kingRank = blackKingLocation.getRank();
        }

        for (int initFile = 0; initFile < 8; initFile++) {
            for (int initRank = 0; initRank < 8; initRank++) {
                if (isValidPieceMove(initFile, initRank, kingFile, kingRank, gameBoard)) {
                    return true;
                }
            }
        }

        return false;
    }
    //test
    public void testCheck() {
        changePlayer();
        if (isKingInCheck(board, !isWhitesMove())) {
            setCheck();
        }
        else {
            unsetCheck();
        }
    }

    public void setCheck() {
        isInCheck = true;
        changePlayer();
        testForCheckmate();
    }

    public void testForCheckmate() {
        List<Pair<Location, Location>> allMoves = getAllValidMoves();
        isCheckmate = allMoves.isEmpty();
    }

    public void testForStalemate() {
        List<Pair<Location, Location>> allMoves = getAllValidMoves();
        isStalemate = allMoves.isEmpty();
    }

    public void unsetCheck() {
        isInCheck = false;
        changePlayer();
        testForStalemate();
    }

    public boolean isWhitesMove() {
        return isWhitesMove;
    }

    public void changePlayer() {
        isWhitesMove = !isWhitesMove;
    }

    public void createClone() {
        clone = createClone(board);
    }

    public Space[][] createClone(Space[][] board) {
        Space[][] clone = new Space[8][8];

        for (int f = 0; f < 8; f++) {
            for (int r = 0; r < 8; r++) {
                try {
                    clone[f][r] = (Space) board[f][r].clone();
                } catch (CloneNotSupportedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return clone;
    }

    public void ai() {

        List<Pair<Location, Location>> allMoves = getAllValidMoves();
        if (allMoves.size() == 0)
            return;

        int randomNum = (int) (Math.random() * allMoves.size());
        Pair<Location, Location> randomMove = allMoves.get(randomNum);
        move(randomMove.first.getFile(), randomMove.first.getRank(),
                randomMove.second.getFile(), randomMove.second.getRank());
    }

    public boolean isCheck() {
        return isInCheck;
    }

    public boolean isCheckmate() {
        return isCheckmate;
    }

    public boolean isStalemate() {
        return isStalemate;
    }

    public List<RecordMove> getRecordedMoves() {
        return recordedMoves;
    }

    public void promotion(int initFile, int initRank, final int finalFile, final int finalRank, Space[][] gameBoard, final RecordMove recordedMove) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final int[] pos = new int[1];

        ChessPiece deletedPawn = gameBoard[finalFile][finalRank].getPiece();

        if (clone.equals(gameBoard)) {
            gameBoard[finalFile][finalRank].setPiece(new Queen(!isWhitesMove()));
            gameBoard[finalFile][finalRank].getPiece().setHasMoved(true);
            recordedMove.setPromotedChessPiece(gameBoard[finalFile][finalRank].getPiece());
        }
        else {
            if(recordedPromotion == false) {
                builder.setTitle(R.string.promotion_title)
                        //.setMessage(R.string.promotion_message)
                        .setItems(R.array.promotion_array, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                pos[0] = which;
                                dialog.dismiss();
                                promote(finalFile, finalRank, pos, recordedMove);
                            }
                        })
                        .setCancelable(false)
                        .show();
            }
            else
                promote(finalFile, finalRank, recordedPromotionPos, recordedMove);

        }

        recordedMove.setDeletedChessPiece(deletedPawn);
        recordedMove.setDeletedFile(initFile);
        recordedMove.setDeletedRank(initRank);
        recordedMove.setPromotion(true);
    }

    public void promote(int f, int r, int[] pos, RecordMove recordedMove) {

        switch(pos[0]) {
            case 0:
                if(recordedPromotion)
                    board[f][r].setPiece(new Queen(isWhitesMove()));
                else
                    board[f][r].setPiece(new Queen(!isWhitesMove()));
                board[f][r].getPiece().setHasMoved(true);
                break;
            case 1:
                if(recordedPromotion)
                    board[f][r].setPiece(new Knight(isWhitesMove()));
                else
                    board[f][r].setPiece(new Knight(!isWhitesMove()));
                board[f][r].getPiece().setHasMoved(true);
                break;
            case 2:
                if(recordedPromotion)
                    board[f][r].setPiece(new Rook(isWhitesMove()));
                else
                    board[f][r].setPiece(new Rook(!isWhitesMove()));
                board[f][r].getPiece().setHasMoved(true);
                break;
            case 3:
                if(recordedPromotion)
                    board[f][r].setPiece(new Bishop(isWhitesMove()));
                else
                    board[f][r].setPiece(new Bishop(!isWhitesMove()));
                board[f][r].getPiece().setHasMoved(true);
                break;
            default:
                break;
        }

        recordedMove.setPromotedChessPiece(board[f][r].getPiece());

    }

    public void setRecordedGamePromotion(ChessPiece chessPiece) {
        recordedPromotion = true;
        if(chessPiece.getClass().equals(Queen.class))
        {
            recordedPromotionPos[0] = 0;
        }
        else if(chessPiece.getClass().equals(Knight.class))
        {
            recordedPromotionPos[0] = 1;
        }
        else if(chessPiece.getClass().equals(Rook.class))
        {
            recordedPromotionPos[0] = 2;
        }
        else if(chessPiece.getClass().equals(Bishop.class))
        {
            recordedPromotionPos[0] = 3;
        }


    }
}
