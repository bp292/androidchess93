package chess93.androidchess93.model;

import chess93.androidchess93.R;

public class Rook extends MoveLong {

    public Rook(boolean isWhite)
    {
        super(isWhite);
    }

    @Override
    public boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {
        if ( ((initFile == finalFile && initRank != finalRank) || (initFile != finalFile && initRank == finalRank))
                && !hasPiecesInbetween(initFile, initRank, finalFile, finalRank, board)) {
            if (board[finalFile][finalRank].getPiece() == null) {
                return true;
            }
            if (board[finalFile][finalRank].getPiece().isWhite() != board[initFile][initRank].getPiece().isWhite()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int getDrawable() {
        if (isWhite()) {
            return R.drawable.ic_white_rook;
        }
        else {
            return R.drawable.ic_black_rook;
        }
    }
}
