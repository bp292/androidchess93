package chess93.androidchess93.model;

import chess93.androidchess93.R;

public class Knight extends ChessPiece {

    public Knight(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {

        if (Math.abs(initFile - finalFile) == 1 && Math.abs(initRank - finalRank) == 2 && board[finalFile][finalRank].getPiece() == null) {
            return true;
        }
        else if (Math.abs(initFile - finalFile) == 2 && Math.abs(initRank - finalRank) == 1 && board[finalFile][finalRank].getPiece() == null) {
            return true;
        }
        else if (Math.abs(initFile - finalFile) == 1 && Math.abs(initRank - finalRank) == 2
                && board[finalFile][finalRank].getPiece().isWhite() != board[initFile][initRank].getPiece().isWhite()) {
            return true;
        }
        else return Math.abs(initFile - finalFile) == 2 && Math.abs(initRank - finalRank) == 1
                    && board[finalFile][finalRank].getPiece().isWhite() != board[initFile][initRank].getPiece().isWhite();
    }

    @Override
    public int getDrawable() {
        if (isWhite()) {
            return R.drawable.ic_white_knight;
        }
        else {
            return R.drawable.ic_black_knight;
        }
    }
}
