package chess93.androidchess93.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

public class RecordGame implements Serializable {

    private List<RecordMove> recordedMoves;
    private String title;
    private Calendar date;

    public RecordGame(String title, Calendar date, List<RecordMove> recordedMoves) {
        this.title = title;
        this.date = date;
        this.recordedMoves = recordedMoves;
    }

    public String getTitle()
    {
        return title;
    }

    public Calendar getDate(){
        return date;
    }
    public List<RecordMove> getRecordedMoves()
    {
        return recordedMoves;
    }
    public String toString() {

        return title + ":\n     " + date.getTime().toString();
    }

}
