package chess93.androidchess93.model;

public abstract class MoveLong extends ChessPiece {

    public MoveLong(boolean isWhite) {
        super(isWhite);
    }

    public boolean hasPiecesInbetween(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {

        int f, r;
        int fileDif, rankDif;

        if (initRank == finalRank && initFile != finalFile) {
            //right
            if (initFile < finalFile)
                for (f = initFile+1; f < finalFile; f++)
                    if (board[f][initRank].hasPiece())
                        return true;
            //left
            if (initFile > finalFile)
                for (f = initFile-1; f > finalFile; f--)
                    if (board[f][initRank].hasPiece())
                        return true;
        }

        if (initRank != finalRank && initFile == finalFile) {
            //upwards
            if (initRank < finalRank)
                for (r = initRank+1; r < finalRank; r++)
                    if (board[initFile][r].hasPiece())
                        return true;
            //downwards
            if (initRank > finalRank)
                for (r = initRank-1; r > finalRank; r--)
                    if (board[initFile][r].hasPiece())
                        return true;
        }

        rankDif = initRank - finalRank;
        fileDif = initFile - finalFile;
        if (Math.abs(rankDif) == Math.abs(fileDif)) {
            // up-right
            if (fileDif < 0 && rankDif < 0) {
                r = initRank+1;
                for (f = initFile+1; f < finalFile; f++) {
                    if (board[f][r].hasPiece())
                        return true;
                    r++;
                }
            }
            // up-left
            if (fileDif > 0 && rankDif < 0) {
                r = initRank+1;
                for (f = initFile-1; f > finalFile; f--) {
                    if (board[f][r].hasPiece())
                        return true;
                    r++;
                }
            }
            // down-right
            if (fileDif < 0 && rankDif > 0) {
                r = initRank-1;
                for (f = initFile+1; f < finalFile; f++) {
                    if (board[f][r].hasPiece())
                        return true;
                    r--;
                }
            }
            // down-left
            if (fileDif > 0 && rankDif > 0) {
                r = initRank-1;
                for (f = initFile-1; f > finalFile; f--) {
                    if (board[f][r].hasPiece())
                        return true;
                    r--;
                }
            }
        }

        return false;
    }

}
