package chess93.androidchess93.model;

import chess93.androidchess93.R;


public class Pawn extends ChessPiece {


    public Pawn(boolean isWhite) {
        super(isWhite);
        setEnpassant(true);
    }

    @Override
    public boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {

        //straight
        if (initFile == finalFile) {
            if (isWhite() && initRank+1 == finalRank && board[finalFile][finalRank].getPiece() == null){
                setEnpassant(false);
                return true;
            }

            if (isWhite() && initRank == 1 && initRank+2 == finalRank && board[finalFile][finalRank].getPiece() == null) {
                setEnpassant(true);
                return true;
            }

            if (!isWhite() && initRank-1 == finalRank && board[finalFile][finalRank].getPiece() == null)
            {
                setEnpassant(false);
                return true;
            }

            if (!isWhite() && initRank == 6 && initRank-2 == finalRank && board[finalFile][finalRank].getPiece() == null){
                setEnpassant(true);
                return true;
            }
        }

        if (isWhite() && initRank+1 == finalRank && initFile+1 == finalFile && board[initFile+1][initRank+1].getPiece() != null && !board[initFile+1][initRank+1].getPiece().isWhite())
        {
            setEnpassant(false);
            return true;
        }
        if (isWhite() && initRank+1 == finalRank && initFile-1 == finalFile && board[initFile-1][initRank+1].getPiece() != null && !board[initFile-1][initRank+1].getPiece().isWhite())
        {
            setEnpassant(false);
            return true;
        }

        if (!isWhite() && initRank-1 == finalRank && initFile+1 == finalFile && board[initFile+1][initRank-1].getPiece() != null && board[initFile+1][initRank-1].getPiece().isWhite())
        {
            setEnpassant(false);
            return true;
        }
        if (!isWhite() && initRank-1 == finalRank && initFile-1 == finalFile && board[initFile-1][initRank-1].getPiece() != null && board[initFile-1][initRank-1].getPiece().isWhite())
        {
            setEnpassant(false);
            return true;
        }

        if (shouldEnpassant(initFile, initRank, finalFile, finalRank, board)) {
            setEnpassant(false);
            return true;
        }

        return false;
    }

    @Override
    public RecordMove move(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {
        RecordMove recordedMove = new RecordMove(hasMoved(), getEnpassant(),
                initFile, initRank, finalFile, finalRank,
                board[finalFile][finalRank].getPiece(), finalFile, finalRank);

        boolean willEnpassant = shouldEnpassant(initFile, initRank, finalFile, finalRank, board);

        ChessPiece initChessPiece = board[initFile][initRank].getPiece();

        board[finalFile][finalRank].setPiece(initChessPiece);
        board[initFile][initRank].setPiece(null);

        if (initFile != finalFile && willEnpassant) {

            recordedMove.setDeletedRank(initRank);

            if (isWhite() && initRank+1 == finalRank && initFile+1 == finalFile) {
                recordedMove.setDeletedChessPiece(board[initFile + 1][initRank].getPiece());
                recordedMove.setDeletedFile(initFile + 1);
                board[initFile + 1][initRank].setPiece(null);
            }
            if (isWhite() && initRank+1 == finalRank && initFile-1 == finalFile) {
                recordedMove.setDeletedChessPiece(board[initFile - 1][initRank].getPiece());
                recordedMove.setDeletedFile(initFile - 1);
                board[initFile - 1][initRank].setPiece(null);
            }

            if (!isWhite() && initRank-1 == finalRank && initFile+1 == finalFile) {
                recordedMove.setDeletedChessPiece(board[initFile + 1][initRank].getPiece());
                recordedMove.setDeletedFile(initFile + 1);
                board[initFile + 1][initRank].setPiece(null);
            }
            if (!isWhite() && initRank-1 == finalRank && initFile-1 == finalFile) {
                recordedMove.setDeletedChessPiece(board[initFile - 1][initRank].getPiece());
                recordedMove.setDeletedFile(initFile - 1);
                board[initFile - 1][initRank].setPiece(null);
            }
        }
        setHasMoved(true);
        return recordedMove;
    }

    @Override
    public int getDrawable() {
        if (isWhite()) {
            return R.drawable.ic_white_pawn;
        }
        else {
            return R.drawable.ic_black_pawn;
        }
        }

    public boolean shouldEnpassant(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {
        if (isWhite() && initRank == 4 && finalRank == 5 && initFile+1 == finalFile && board[initFile+1][initRank].getPiece() != null && !board[initFile+1][initRank].getPiece().isWhite() && canEnpassant(initFile+1, initRank,board) && board[finalFile][finalRank].getPiece() == null)
        {
            return true;
        }
        if (isWhite() && initRank == 4 && finalRank == 5 && initFile-1 == finalFile && board[initFile-1][initRank].getPiece() != null && !board[initFile-1][initRank].getPiece().isWhite() && canEnpassant(initFile-1, initRank,board) && board[finalFile][finalRank].getPiece() == null)
        {
            return true;
        }
        if (!isWhite() && initRank == 3 && finalRank == 2&& initFile+1 == finalFile && board[initFile+1][initRank].getPiece() != null && board[initFile+1][initRank].getPiece().isWhite() && canEnpassant(initFile+1, initRank,board) && board[finalFile][finalRank].getPiece() == null)
        {
            return true;
        }
        return !isWhite() && initRank == 3 && finalRank == 2 && initFile - 1 == finalFile && board[initFile - 1][initRank].getPiece() != null && board[initFile - 1][initRank].getPiece().isWhite() && canEnpassant(initFile - 1, initRank, board) && board[finalFile][finalRank].getPiece() == null;
    }

    /**
     * canEnpassant
     * Indicates whether pawn can perform enpassant
     * @param file
     * @param rank
     * @param board
     * @return boolean
     */
    public boolean canEnpassant(int file, int rank, Space[][] board)
    {
        boolean wouldBeUnderAttack = false;
        boolean isWhite = board[file][rank].getPiece().isWhite();

        if(isWhite && rank!=0 && file!=7 && board[file + 1][rank].getPiece()!= null && !board[file + 1][rank].getPiece().isWhite()){
            wouldBeUnderAttack = true;
        }
        else if(isWhite && rank!=0 && file!=0 && board[file - 1][rank].getPiece()!= null && !board[file -1][rank].getPiece().isWhite()){
            wouldBeUnderAttack = true;
        }
        else if(!isWhite && rank!=7 && file!=7 && board[file + 1][rank].getPiece()!= null && board[file+1][rank].getPiece().isWhite()){
            wouldBeUnderAttack = true;
        }
        else if(!isWhite && rank!=7 && file!=0 &&board[file - 1][rank].getPiece()!= null && board[file-1][rank].getPiece().isWhite()){
            wouldBeUnderAttack = true;
        }

        return board[file][rank].getPiece().getEnpassant() && wouldBeUnderAttack;
    }
}
