package chess93.androidchess93.model;

import java.io.Serializable;

public abstract class ChessPiece implements Cloneable, Serializable {

    private boolean isWhite;
    private boolean hasMoved;
    private boolean enpassant;

    public ChessPiece(boolean isWhite) {
        this.isWhite = isWhite;
        hasMoved = false;
        enpassant = false;
    }

    /**
     * isWhite Accessor
     * @return true if piece is white, else false
     */
    public boolean isWhite() {
        return isWhite;
    }

    /**
     * hasMoved Accessor
     * @return true if piece has moved, else false
     */
    public boolean hasMoved() {
        return hasMoved;
    }

    public void setHasMoved(boolean hasMoved)
    {
        this.hasMoved = hasMoved;
    }

    /**
     * Check to see if a move is valid
     * @param initFile - The starting file
     * @param initRank - The starting rank
     * @param finalFile - The ending file
     * @param finalRank - The ending rank
     * @param board - the chess board
     * @return true if the move is valid, else false
     */
    public abstract boolean isValidMove(int initFile, int initRank, int finalFile, int finalRank, Space[][] board);

    /**
     * Moves the piece from to the finalFile and finalRank and removes pieces as necessary
     * @param initFile - The starting file
     * @param initRank - The starting rank
     * @param finalFile - The ending file
     * @param finalRank - The ending rank
     * @param board - the chess board
     * @return Pair of the deleted piece and it's integer
     */
    public RecordMove move(int initFile, int initRank, int finalFile, int finalRank, Space[][] board) {

        ChessPiece initChessPiece = board[initFile][initRank].getPiece();

        ChessPiece deadChessPiece = board[finalFile][finalRank].getPiece();

        RecordMove recordedMove = new RecordMove(hasMoved(), getEnpassant(),
                initFile, initRank, finalFile, finalRank,
                deadChessPiece, finalFile, finalRank);

        board[finalFile][finalRank].setPiece(initChessPiece);
        board[initFile][initRank].setPiece(null);

        setHasMoved(true); // set hasMoved to true

        return recordedMove;

    }

    /**
     * getEnpassant()
     * @return boolean
     *
     * Returns boolean indicating whether this piece can be captured by enpassant
     */
    public boolean getEnpassant()
    {
        return enpassant;
    }

    /**
     * setEnpassant()
     * @param e - new enpassant value
     */
    public void setEnpassant(boolean e) {
        enpassant = e;
    }

    /**
     * Depending on the piece, return the correct drawable id corresponding to the piece and color
     * @return the drawable id corresponding to the piece and color oombination
     */
    public abstract int getDrawable();

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
