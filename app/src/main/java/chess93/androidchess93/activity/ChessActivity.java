package chess93.androidchess93.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Calendar;

import chess93.androidchess93.R;
import chess93.androidchess93.adapter.BoardAdapter;
import chess93.androidchess93.model.Game;
import chess93.androidchess93.model.RecordGame;
import chess93.androidchess93.model.RecordGameList;

public class ChessActivity extends AppCompatActivity {

    private final String WHITE_TURN = "White's Turn";
    private final String BLACK_TURN = "Black's Turn";

    private GridView chessboardGrid;
    private Game board;
    private TextView playerTurnTextView;
    private Button undoBtn, aiBtn, resignBtn, drawBtn;

    private boolean isResign, isDraw, isDrawConfirmed;

    private RecordGame recordedGame = null;

    RecordGameList games = new RecordGameList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

      try
       {
           games = RecordGameList.read(this);
       }
       catch(Exception e)
        {
            e.printStackTrace();
        }

        if(games == null)
        {
            games = new RecordGameList();
        }
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chess);

        playerTurnTextView = (TextView) findViewById(R.id.playerTurnTextView);
        undoBtn = (Button) findViewById(R.id.undoBtn);
        aiBtn = (Button) findViewById(R.id.aiBtn);
        resignBtn = (Button) findViewById(R.id.resignBtn);
        drawBtn = (Button) findViewById(R.id.drawBtn);

        playerTurnTextView.setText(WHITE_TURN);

        undoBtn.setEnabled(false);

        isResign = false;
        isDraw = false;

        board = new Game(this);

        chessboardGrid = (GridView) findViewById(R.id.chessboardGrid);

        BoardAdapter adapter = new BoardAdapter(this, board);

        chessboardGrid.setAdapter(adapter);

    }

    public void undo(View view) {
        board.undo(board.getBoard());
        changePlayerTurnText();
        board.createClone();
        undoBtn.setEnabled(false);
    }

    public void ai(View view) {
        board.ai();
        changePlayerTurnText();
        undoBtn.setEnabled(true);

        resetDraw();

        if (board.isCheck() || board.isCheckmate() || board.isStalemate())
            showAlert();
    }

    public void resign(View view) {
        isResign = true;
        if(board.getRecordedMoves().isEmpty())
           finish();
        else
            showAlert();
    }

    public void draw(View view) {
        if (isDraw) {
            isDrawConfirmed = true;
            if(board.getRecordedMoves().isEmpty())
                finish();
            else
                showAlert();
        }
        else {
            isDraw = true;
            drawBtn.setText(R.string.draw_confirm);
        }

    }

    public void move(int initFile, int initRank, int finalFile, int finalRank) {
        if (board.isValidMove(initFile, initRank, finalFile, finalRank)) {
            board.move(initFile, initRank, finalFile, finalRank);
            changePlayerTurnText();
            undoBtn.setEnabled(true);

            resetDraw();

            if (board.isCheck() || board.isCheckmate() || board.isStalemate())
                showAlert();
        }
    }

    public void changePlayerTurnText() {
        playerTurnTextView.setText(board.isWhitesMove() ? WHITE_TURN : BLACK_TURN);
    }

    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (board.isCheck() && !board.isCheckmate()) {
            builder.setTitle(R.string.check_title)
                    .setPositiveButton(R.string.check_ok_dialog, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .show();
            Log.d("Display Alert", "Check!");
        }
        if (board.isCheckmate() || board.isStalemate() || isResign || isDrawConfirmed) {
            undoBtn.setEnabled(false);
            aiBtn.setEnabled(false);
            drawBtn.setEnabled(false);
            resignBtn.setEnabled(false);

            String title;

            if (board.isCheckmate())
                title = getResources().getString(R.string.checkmate_title) + " " + (!board.isWhitesMove() ? "White" : "Black") + " wins!";
            else if (board.isStalemate())
                title = getResources().getString(R.string.stalemate_title);
            else if (isResign)
                title = getResources().getString(R.string.resign_title) + " " + (!board.isWhitesMove() ? "White" : "Black") + " wins!";
            else
                title = getResources().getString(R.string.draw_title);

            final EditText titleInput = new EditText(this);
            titleInput.setInputType(InputType.TYPE_CLASS_TEXT);
            titleInput.setHint(R.string.dialog_title_hint);

            builder.setTitle(title)
                    .setMessage(R.string.dialog_save_match_message)
                    .setView(titleInput)
                    .setPositiveButton(R.string.dialog_save_match_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //nothing
                        }
                    })
                    .setNegativeButton(R.string.dialog_cancel_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setCancelable(false);

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (!titleInput.getText().toString().trim().isEmpty()) {
                        recordedGame = new RecordGame(titleInput.getText().toString(),
                                Calendar.getInstance(), board.getRecordedMoves());

                        if (!recordedGame.getRecordedMoves().isEmpty()) {
                            recordedGame.getRecordedMoves().get(recordedGame.getRecordedMoves().size()-1).setResign(isResign);
                            recordedGame.getRecordedMoves().get(recordedGame.getRecordedMoves().size()-1).setDraw(isDrawConfirmed);
                        }

                           games.addGameToList(recordedGame);
                        try {
                            RecordGameList.write(getApplicationContext(), games);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        finish();
                    }
                }
            });

        }
    }

    public void resetDraw() {
        isDraw = false;
        drawBtn.setText(R.string.draw_btn);
    }
}
