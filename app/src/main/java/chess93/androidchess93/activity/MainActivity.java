package chess93.androidchess93.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import chess93.androidchess93.R;
import chess93.androidchess93.model.RecordGameList;

public class MainActivity extends AppCompatActivity {

    RecordGameList games;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }

        Log.d("Height of titlebar", result +"");

    }

    public void playChess(View view) {
        Intent i = new Intent(this, ChessActivity.class);
        startActivity(i);
    }

    public void viewRecordedGames(View view) {
        Intent i = new Intent(this, RecordActivity.class);
        startActivity(i);
    }
}
