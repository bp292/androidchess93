package chess93.androidchess93.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.Serializable;
import java.util.Comparator;

import chess93.androidchess93.R;
import chess93.androidchess93.model.RecordGame;
import chess93.androidchess93.model.RecordGameList;

public class RecordActivity extends AppCompatActivity {

    RecordGameList games;
    ListView listView;
    Comparator<RecordGame> byDate;
    Comparator<RecordGame> byTitle;
    ArrayAdapter<RecordGame> adapter;
    RecordGame selectedFromList = null;
    Button playback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded_games);

        playback= (Button)findViewById(R.id.button);
        playback.setEnabled(false);

        try {
            games = RecordGameList.read(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(games==null)
        {
            games = new RecordGameList();
        }
        adapter = new ArrayAdapter<RecordGame>(this, android.R.layout.simple_list_item_1, games.getGamesList());
        //spicy lambda
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                listView.setSelection(position);
                view.setSelected(true);
                selectedFromList = (RecordGame) listView.getItemAtPosition(position);
                playback.setEnabled(true);
            }
        });

        byDate = new Comparator<RecordGame>() {
            @Override
            public int compare(RecordGame object1, RecordGame object2) {
                return object1.getDate().compareTo(object2.getDate());
            }
        };

        byTitle = new Comparator<RecordGame>() {
            public int compare(RecordGame object1, RecordGame object2) {
                return object1.getTitle().compareTo(object2.getTitle());
            }
        };

        adapter.sort(byTitle);
    }

    public void sortByDate(View view) {
        adapter.sort(byDate);
    }

    public void sortByTitle(View view) {
        adapter.sort(byTitle);
    }

    public void playbackGame(View view) {
        Intent i = new Intent(this, PlaybackActivity.class);
        i.putExtra("SelectedGame", selectedFromList);
        System.out.println(selectedFromList.getTitle());
        startActivity(i);
    }

}
