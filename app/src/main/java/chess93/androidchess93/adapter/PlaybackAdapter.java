package chess93.androidchess93.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import chess93.androidchess93.activity.PlaybackActivity;
import chess93.androidchess93.model.Game;
import chess93.androidchess93.model.Space;

public class PlaybackAdapter extends BaseAdapter {

    private PlaybackActivity playbackActivity;
    private Context context;
    private Space[][] board;
    private Game game;
    private int itemSelected;

    android.widget.GridView.LayoutParams layoutParams;

    public PlaybackAdapter(Context context, Game game) {
        this.context = context;
        this.board = game.getBoard();
        this.game = game;
        itemSelected = -1;
        playbackActivity = (PlaybackActivity) context;
    }

    @Override
    public Object getItem(int position) {
        int file = position % 8;
        int rank = Math.abs((position - file)/8 - 7);
        return getItem(file, rank);
    }

    @Override
    public int getCount() {
        return 64;
    }

    public int getRowCount() {
        return 8;
    }

    public int getColumnCount() {
        return 8;
    }


    public Object getItem(int file, int rank) {
        return board[file][rank];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Space getView(final int position, View view, final ViewGroup parent) {
        final int file = position % 8;
        final int rank = Math.abs((position - file)/8 - 7);

        final Space img = board[file][rank];
        img.setTag(file + " " + rank);



        int barHeight = ((Activity) context).getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();


        return board[file][rank];
    }
}